db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "supplier_id", total: {$sum: "$stock"}}}
]);


db.fruits.aggregate([
    {$match: {stock: {$gt:20}}},
    {$group: {_id: "supplier_id", total: {$sum: "$stock"}}}
]);


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avgAmount: { $avg: { $sum: "$price"}}}}
]);


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", maxTotalAmount: { $max: { $sum: "$price"}}}}
]);


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", minTotalAmount: { $min: { $sum: "$price"}}}}
]);